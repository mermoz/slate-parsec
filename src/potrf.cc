//------------------------------------------------------------------------------
// Copyright (c) 2017, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This research was supported by the Exascale Computing Project (17-SC-20-SC),
// a collaborative effort of two U.S. Department of Energy organizations (Office
// of Science and the National Nuclear Security Administration) responsible for
// the planning and preparation of a capable exascale ecosystem, including
// software, applications, hardware, advanced system engineering and early
// testbed platforms, in support of the nation's exascale computing imperative.
//------------------------------------------------------------------------------
// For assistance with SLATE, email <slate-user@icl.utk.edu>.
// You can also join the "SLATE User" Google group by going to
// https://groups.google.com/a/icl.utk.edu/forum/#!forum/slate-user,
// signing in with your Google credentials, and then clicking "Join group".
//------------------------------------------------------------------------------

#include "slate/slate.hh"
#include "aux/Debug.hh"
#include "slate/Matrix.hh"
#include "slate/HermitianMatrix.hh"
#include "slate/TriangularMatrix.hh"
#include "internal/internal.hh"
#if defined SLATE_WITH_PARSEC
#include "slate/parsec/tasks.hh"
#include "slate/parsec/PanelBcastTask.hh"
#include "slate/parsec/PotrfTask.hh"
#include "slate/parsec/PotrfPanelTask.hh"
#include "slate/parsec/PotrfSolveTask.hh"
#include "slate/parsec/PotrfUpdateDiagTask.hh"
#include "slate/parsec/PotrfUpdate.hh"
#include "slate/parsec/PotrfUpdateTask.hh"
#include "slate/parsec/PotrfUpdateTrailingTask.hh"
#include "slate/parsec/TileBcast.hh"
#endif

namespace slate {

// specialization namespace differentiates, e.g.,
// internal::potrf from internal::specialization::potrf
namespace internal {
namespace specialization {

//------------------------------------------------------------------------------
/// Distributed parallel Cholesky factorization.
/// Generic implementation for any target.
/// Panel and lookahead computed on host using Host OpenMP task.
/// @ingroup posv_specialization
///
template <Target target, typename scalar_t>
void potrf(slate::internal::TargetType<target>,
           HermitianMatrix<scalar_t> A, int64_t lookahead)
{
    using real_t = blas::real_type<scalar_t>;
    using BcastList = typename Matrix<scalar_t>::BcastList;

    // Assumes column major
    const Layout layout = Layout::ColMajor;

    // if upper, change to lower
    if (A.uplo() == Uplo::Upper) {
        A = conjTranspose(A);
    }
    const int64_t A_nt = A.nt();

    // OpenMP needs pointer types, but vectors are exception safe
    std::vector< uint8_t > column_vector(A_nt);
    uint8_t* column = column_vector.data();

    #pragma omp parallel
    #pragma omp master
    {
        omp_set_nested(1);
        for (int64_t k = 0; k < A_nt; ++k) {
            // panel, high priority
            #pragma omp task depend(inout:column[k]) priority(1)
            {
                // factor A(k, k)
                internal::potrf<Target::HostTask>(A.sub(k, k), 1);

                // send A(k, k) down col A(k+1:nt-1, k)
                if (k+1 <= A_nt-1)
                    A.tileBcast(k, k, A.sub(k+1, A_nt-1, k, k), layout);

                // A(k+1:nt-1, k) * A(k, k)^{-H}
                if (k+1 <= A_nt-1) {
                    auto Akk = A.sub(k, k);
                    auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
                    internal::trsm<Target::HostTask>(
                        Side::Right,
                        scalar_t(1.0), conjTranspose(Tkk),
                        A.sub(k+1, A_nt-1, k, k), 1);
                }

                BcastList bcast_list_A;
                for (int64_t i = k+1; i < A_nt; ++i) {
                    // send A(i, k) across row A(i, k+1:i) and down col A(i:nt-1, i)
                    bcast_list_A.push_back({i, k, {A.sub(i, i, k+1, i),
                                                   A.sub(i, A_nt-1, i, i)}});
                }
                A.template listBcast(bcast_list_A, layout);
            }
            // update lookahead column(s), high priority
            for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) {
                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[j]) priority(1)
                {
                    // A(j, j) -= A(j, k) * A(j, k)^H
                    internal::herk<Target::HostTask>(
                        real_t(-1.0), A.sub(j, j, k, k),
                        real_t( 1.0), A.sub(j, j), 1);

                    // A(j+1:nt-1, j) -= A(j+1:nt-1, k) * A(j, k)^H
                    if (j+1 <= A_nt-1) {
                        auto Ajk = A.sub(j, j, k, k);
                        internal::gemm<Target::HostTask>(
                            scalar_t(-1.0), A.sub(j+1, A_nt-1, k, k),
                                            conjTranspose(Ajk),
                            scalar_t(1.0), A.sub(j+1, A_nt-1, j, j),
                            layout, 1);
                    }
                }
            }
            // update trailing submatrix, normal priority
            if (k+1+lookahead < A_nt) {
                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[k+1+lookahead]) \
                                 depend(inout:column[A_nt-1])
                {
                    // A(kl+1:nt-1, kl+1:nt-1) -=
                    //     A(kl+1:nt-1, k) * A(kl+1:nt-1, k)^H
                    // where kl = k + lookahead
                    internal::herk<target>(
                        real_t(-1.0), A.sub(k+1+lookahead, A_nt-1, k, k),
                        real_t( 1.0), A.sub(k+1+lookahead, A_nt-1));
                }
            }
        }

        // TODO: causes issues on summit Target::HostTask
        // #pragma omp taskwait
        // A.tileUpdateAllOrigin();
    }

    // Debug::checkTilesLives(A);
    // Debug::printTilesLives(A);
    A.tileUpdateAllOrigin();
    A.releaseWorkspace();
}

//------------------------------------------------------------------------------
/// An auxiliary routine to release the panel tiles that are broadcasted. Since
/// the broadcasted tiles are flagged to be hold on the devices memories to be
/// accessed by multiple internal kernels while preventing the tileRelease call
/// in these routine to release them before the others finish accessing
/// them. Note: this function update the tiles origin to make sure that
/// the origin memory is up-to-date and the coherency is kept consistent
/// across multiple address spaces.
/// @param[in] A
///     The n-by-n Hermitian positive definite matrix $A$, which is
///     a sub of the input matrix $A$.
///
/// @param[in] k
///     Current column k of the input matrix $A$.
///
/// @ingroup posv_computational
///
template <typename scalar_t>
void potrfReleasePanel(HermitianMatrix<scalar_t> A, int64_t k)
{
    const int64_t A_nt = A.nt();
    for (int64_t i = k+1; i < A_nt; ++i) {
        if (A.tileIsLocal(i, k)) {
            A.tileUpdateOrigin(i, k);

            std::set<int> dev_set;
            A.sub(i, i, k+1, i).getLocalDevices(&dev_set);
            A.sub(i, A_nt-1, i, i).getLocalDevices(&dev_set);

            for (auto device : dev_set) {
                A.tileUnsetHold(i, k, device);
                A.tileRelease(i, k, device);
            }
        }
    }
}

//------------------------------------------------------------------------------
/// Distributed parallel Cholesky factorization.
/// GPU device batched cuBLAS implementation.
/// @ingroup posv_specialization
///
template <typename scalar_t>
void potrf(slate::internal::TargetType<Target::Devices>,
           HermitianMatrix<scalar_t> A, int64_t lookahead)
{
    using real_t = blas::real_type<scalar_t>;
    using BcastList = typename Matrix<scalar_t>::BcastList;

    // Assumes column major
    const Layout layout = Layout::ColMajor;

    // if upper, change to lower
    if (A.uplo() == Uplo::Upper) {
        A = conjTranspose(A);
    }
    const int64_t A_nt = A.nt();

    // OpenMP needs pointer types, but vectors are exception safe
    std::vector< uint8_t > column_vector(A_nt);
    uint8_t* column = column_vector.data();

    const int priority_zero = 0;
    const int tag_zero = 0;
    const int life_factor_one = 1;
    const bool is_shared = lookahead > 0;
    const int batch_arrays_index_one = 1;
    const int64_t batch_size_zero = 0;
    const int64_t num_arrays_two  = 2; // Number of kernels without lookahead

    // Allocate batch arrays = number of kernels without lookahead + lookahead
    // number of kernels without lookahead = 2 (internal::gemm & internal::trsm)
    // whereas internal::herk will be executed as many as lookaheads, thus
    // internal::herk needs batch arrays equal to the number of lookaheads
    // and the batch_arrays_index starts from
    // the number of kernels without lookahead, and then incremented by 1
    // for every execution for the internal::herk
    A.allocateBatchArrays(batch_size_zero, (num_arrays_two + lookahead));
    A.reserveDeviceWorkspace();

    #pragma omp parallel
    #pragma omp master
    {
        omp_set_nested(1);
        for (int64_t k = 0; k < A_nt; ++k) {
            // Panel, normal priority
            #pragma omp task depend(inout:column[k])
            {
                // factor A(k, k)
                internal::potrf<Target::HostTask>(A.sub(k, k));

                // send A(k, k) down col A(k+1:nt-1, k)
                if (k+1 <= A_nt-1)
                    A.tileBcast(k, k, A.sub(k+1, A_nt-1, k, k), layout);

                // A(k+1:nt-1, k) * A(k, k)^{-H}
                if (k+1 <= A_nt-1) {
                    auto Akk = A.sub(k, k);
                    auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
                    internal::trsm<Target::Devices>(
                        Side::Right,
                        scalar_t(1.0), conjTranspose(Tkk),
                                       A.sub(k+1, A_nt-1, k, k),
                        priority_zero, layout, batch_arrays_index_one);
                }

                BcastList bcast_list_A;
                for (int64_t i = k+1; i < A_nt; ++i) {
                    // send A(i, k) across row A(i, k+1:i) and
                    //                down col A(i:nt-1, i)
                    bcast_list_A.push_back({i, k, {A.sub(i, i, k+1, i),
                                                   A.sub(i, A_nt-1, i, i)}});
                }

                // "is_shared" is to request copying the tiles to the devices,
                // and set them on-hold, which avoids releasing them by either
                // internal::gemm or internal::herk
                // (avoiding possible race condition)
                A.template listBcast<Target::Devices>(
                  bcast_list_A, layout, tag_zero, life_factor_one, is_shared);
            }

            // update trailing submatrix, normal priority
            if (k+1+lookahead < A_nt) {
                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[k+1+lookahead]) \
                                 depend(inout:column[A_nt-1])
                {
                    // A(kl+1:nt-1, kl+1:nt-1) -=
                    //     A(kl+1:nt-1, k) * A(kl+1:nt-1, k)^H
                    // where kl = k + lookahead
                    internal::herk<Target::Devices>(
                        real_t(-1.0), A.sub(k+1+lookahead, A_nt-1, k, k),
                        real_t( 1.0), A.sub(k+1+lookahead, A_nt-1));
                }
            }

            // update lookahead column(s), normal priority
            // the batch_arrays_index_la must be initialized to the
            // lookahead base index (i.e, number of kernels without lookahead),
            // which is equal to "2" for slate::potrf, and then the variable is
            // incremented with every lookahead column "j" ( j-k+1 = 2+j-(k+1) )
            for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) {
                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[j])
                {
                    // A(j, j) -= A(j, k) * A(j, k)^H
                    internal::herk<Target::Devices>(
                        real_t(-1.0), A.sub(j, j, k, k),
                        real_t( 1.0), A.sub(j, j));

                    // A(j+1:nt, j) -= A(j+1:nt-1, k) * A(j, k)^H
                    if (j+1 <= A_nt-1) {
                        auto Ajk = A.sub(j, j, k, k);
                        internal::gemm<Target::Devices>(
                            scalar_t(-1.0), A.sub(j+1, A_nt-1, k, k),
                                            conjTranspose(Ajk),
                            scalar_t( 1.0), A.sub(j+1, A_nt-1, j, j),
                            layout, priority_zero, j-k+1);
                    }
                }
            }

            // update the status of the on-hold tiles held by the invocation of
            // the tileBcast routine, and then release them to free up memory
            // the origin must be updated with the latest modified copy.
            // for memory consistency
            // TODO: find better solution to handle tile release, and
            //       investigate the correctness of the task dependency
            if (lookahead > 0 && k >= lookahead) {
                #pragma omp task depend(in:column[k]) \
                                 depend(inout:column[k+1])
                {
                    potrfReleasePanel(A, k - lookahead);
                }
            }
        }

        #pragma omp taskwait
        A.tileUpdateAllOrigin();
    }

    A.releaseWorkspace();
}

#if defined SLATE_WITH_PARSEC

template <typename scalar_t>
void potrf_ctlflow_flat(
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   std::string context = "potrf_ctlflow_flat";

   // Assumes column major
   // const Layout layout = Layout::ColMajor;

   // if upper, change to lower
   if (A.uplo() == Uplo::Upper) {
      A = conj_transpose(A);
   }
   const int64_t A_nt = A.nt();
   std::cout << "[" << context << "] my rank = " << A.mpiRank() << ", nt = " << A_nt << std::endl;

   // Debug
   // if (0 == A.mpiRank()) {
   // if (1 == A.mpiRank()) {
   //    sleep(1);
   // }

   int rc; // Parsec error code
   rc = parsec_context_start(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_start");

   using matrix_t = HermitianMatrix<scalar_t>;

   // A.enqueue_taskpool(A.parsec_potrf_panel_tp);

   for (int64_t k = 0; k < A_nt; ++k) {
      // for (int64_t k = 0; k < 1; ++k) {

      // std::cout << "[" << context << "] k = " << k << std::endl;

      // std::cout << "[potrf Target::Parsec] Is tile local = " << A.tileIsLocal(k, k) << std::endl;
      // std::cout << "[potrf Target::Parsec] PRE myrank = " << A.mpiRank()
      //           << " tp->taskpool_id = " << tp->taskpool_id
      //           << " tp->nb_tasks = " << tp->nb_tasks << std::endl;

      //
      // Potrf panel
      //

      //
      // Potrf A(k,k)

      {
         // Task type
         using PotrfTask = parsec::PotrfTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfTask::Arguments potrf_args(k, &A);
         // Initialize task 
         PotrfTask potrf_task;
         potrf_task.initialize(potrf_args);
         
         // potrf_task.execute(); // Synchronous execution
         // internal::potrf<Target::HostTask>(A.sub(k, k), 1);

         // Asynchronous execution (submitted to high_level taskpool)
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         potrf_task.submit(tp);
         parsec_dtd_taskpool_wait(tp);      

         // parsec::potrf_panel_task<matrix_t, scalar_t>(A, k, A.column(k));
         // parsec::potrf_task<matrix_t, scalar_t>(A, k);
      }

      // Send diagonal tile A(k, k) to sub diagonal tile A(k+1:nt, k)
      if (k+1 < A_nt) {

         parsec_taskpool_t* tp = A.parsec_high_level_tp;

         using SubDiagBcast = parsec::TileBcastTask<matrix_t, scalar_t>;
         typename SubDiagBcast::Arguments bcast_args(k, k, {k+1, A_nt-1, k, k}, &A);
         SubDiagBcast bcast_task;
         bcast_task.initialize(bcast_args);
         // bcast_task.execute();
         
         bcast_task.submit(tp);

         auto* akk_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), k, k)];
         parsec_dtd_data_flush(tp, akk_dtd_tile);

         parsec_dtd_taskpool_wait(tp);
      }
      
      //
      // Trsm A(k+1:A_nt,k)

      if (k+1 <= A_nt-1) {

         for (int64_t i = k+1; i < A_nt; ++i) {

            //
            // Trsm A(i,k)
            {
               // Task type
               using TrsmTask = parsec::PotrfSolveTask<matrix_t, scalar_t>; 
               // Task arguments
               typename TrsmTask::Arguments trsm_args(i, k, &A);

               TrsmTask trsm_task;
               trsm_task.initialize(trsm_args);

               // trsm_task.execute(); // Synchronous execution

               // Asynchronous execution (submitted to high_level taskpool)
               trsm_task.submit(A.parsec_high_level_tp);
            }

         }

         //    // auto Akk = A.sub(k, k);
         //    // auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
         //    // internal::trsm<Target::HostTask>(
         //    //       Side::Right,
         //    //       scalar_t(1.0), conj_transpose(Tkk),
         //    //       A.sub(k+1, A_nt-1, k, k), 1);

         parsec_dtd_taskpool_wait(A.parsec_high_level_tp);      
      }

      // Send tiles in current block-column to trailing submatrix
      if (k+1 < A_nt) {

         parsec_taskpool_t* tp = A.parsec_high_level_tp;

         //    parsec::Amk_comm_task<matrix_t, scalar_t>(
         //          A, k,
         //          A.column(k), //INPUT
         //          A.column(k+1), //INOUT
         //          A.column(A.nt()-1)); //INOUT

         for (int64_t i = k+1; i < A_nt; ++i) {
            
            using TrailingBcast = parsec::TileBcastTask<matrix_t, scalar_t>;
            // Send A(i,k) to A(i, k+1:i) and to A(i+1:nt, i)
            typename TrailingBcast::Arguments bcast1_args(i, k, {{i, i, k+1, i}, {i+1, A_nt-1, i, i}}, &A);
            TrailingBcast bcast1_task;
            bcast1_task.initialize(bcast1_args);
            // bcast1_task.execute();

            bcast1_task.submit(tp);
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);

            // Send A(i,k) to A(i+1:nt, i)

            // // typename TrailingBcast::Arguments bcast2_args(i, k, {i, A_nt-1, i, i}, &A);
            // typename TrailingBcast::Arguments bcast2_args(i, k, {i+1, A_nt-1, i, i}, &A);
            // TrailingBcast bcast2_task;
            // bcast2_task.initialize(bcast2_args);
            // // bcast2_task.execute();
            // bcast2_task.submit(tp);
            // // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            // Flush A(i,k) tile
            auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
            parsec_dtd_data_flush(tp, aik_dtd_tile);
         }

         parsec_dtd_taskpool_wait(tp);

         std::cout << "[" << context << "] myrank = " << A.mpiRank() << " Bcast done" << std::endl;
      }

      // 
      // Update trailing submatrix
      //
      
      if (k+1+lookahead < A_nt) {
         // if (false) {

         // Parsec taskpool
         parsec_taskpool_t * tp = A.parsec_high_level_tp;

         //    parsec::potrf_trailing_matrix_task<matrix_t, scalar_t>(
         //          A, k, k+1+lookahead,
         //          A.column(k), //INPUT
         //          A.column(k+1+lookahead), //INOUT
         //          A.column(A.nt()-1)); //INOUT
         
         for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

            //
            // Update diagonal tile in column `j`

            // Debug
            // if (0 == A.mpiRank()) {
            //    sleep(1);
            // }
            
            // Task type
            using UpdateDiagTask = parsec::PotrfUpdateDiagTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDiagTask::Arguments upd_diag_args(k, j, &A);

            UpdateDiagTask upd_diag_task;
            upd_diag_task.initialize(upd_diag_args);
            
            // upd_diag_task.execute(); // Synchronous execution

            upd_diag_task.submit(tp); // Synchronous execution
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            //
            // Update sub-diagonal tile in column `j`

            for (int64_t i = j+1; i < A_nt; ++i) {

               // Task type
               using UpdateTask = parsec::PotrfUpdateTask<matrix_t, scalar_t>; 
               // Task arguments
               typename UpdateTask::Arguments upd_args(k, i, j, &A);

               UpdateTask upd_task;
               upd_task.initialize(upd_args);

               // upd_task.execute(); // Synchronous execution
               upd_task.submit(tp); // Synchronous execution
  
            }
         }

         parsec_dtd_taskpool_wait(tp);
      }
      
      // Lookahead column(s)
      for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) { // lookahead column(s)

         // Parsec taskpool
         parsec_taskpool_t * tp = A.parsec_high_level_tp;

         // Task type
         using UpdateDiagTask = parsec::PotrfUpdateDiagTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateDiagTask::Arguments upd_diag_args(k, j, &A);

         UpdateDiagTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         upd_diag_task.submit(tp); // Synchronous execution

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {
            // Task type
            using UpdateTask = parsec::PotrfUpdateTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateTask::Arguments upd_args(k, i, j, &A);

            UpdateTask upd_task;
            upd_task.initialize(upd_args);

            // upd_task.execute(); // Synchronous execution
            upd_task.submit(tp); // Synchronous execution
         }

         parsec_dtd_taskpool_wait(tp);

      }

      // parsec_dtd_data_flush(A.parsec_high_level_tp, A.column(k));

      // for (int64_t j = k; j < A_nt; ++j)
      // parsec_dtd_data_flush(A.parsec_high_level_tp, A.column(j));

      // std::cout << "[potrf Target::Parsec] my rank = " << A.mpiRank()
      // << ", flushed all columns" << std::endl;
      
   }

   // for (int64_t k = 0; k < A_nt; ++k) 
   //    parsec_dtd_data_flush(A.parsec_high_level_tp, A.column(k));
   
   // std::cout << "[potrf Target::Parsec] myrank = " << A.mpiRank() << " tp->nb_tasks = " << tp->nb_tasks << std::endl;

   std::cout << "[potrf_ctlflow_flat] myrank = " << A.mpiRank() << ", wait for completion (parsec_high_level_tp).." << std::endl;
   rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);
   PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
   std::cout << "[potrf Target::Parsec] wait for completion (parsec_context).." << std::endl;

   rc = parsec_context_wait(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_wait");
   std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank() << ", execution done" << std::endl;

   A.tileUpdateAllOrigin();
   A.releaseWorkspace();

}

template <typename scalar_t>
void potrf_ctlflow_nested(
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   std::string context = "potrf_ctlflow_nested";

   // Alias
   using matrix_t = HermitianMatrix<scalar_t>;

   // if upper, change to lower
   if (A.uplo() == Uplo::Upper) {
      A = conj_transpose(A);
   }
   const int64_t A_nt = A.nt();
   std::cout << "[" << context << "] my rank = " << A.mpiRank() << ", nt = " << A_nt << std::endl;

   // Debug
   // if (0 == A.mpiRank()) {
   // if (1 == A.mpiRank()) {
   //    sleep(1);
   // }

   int rc; // Parsec error code
   rc = parsec_context_start(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_start");

   for (int64_t k = 0; k < A_nt; ++k) {
   // for (int64_t k = 0; k < 1; ++k) {

      // std::cout << "[" << context << "] k = " << k << std::endl;

      //
      // Potrf Panel A(:,k)
      //
      
      {
         // Task type
         using PotrfPanelTask = parsec::PotrfPanelTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfPanelTask::Arguments potrf_panel_args(k, &A);
         // Initialize task 
         PotrfPanelTask potrf_panel_task;
         potrf_panel_task.initialize(potrf_panel_args);

         // potrf_panel_task.execute();
         potrf_panel_task.submit(A.parsec_high_level_tp);
      }

      //
      // Broadcast Panel A(:,k) to trailing submatrix
      //
      
      // // Broadcast panel tiles
      // if (k+1 < A_nt) {
         
      //    // Task type
      //    using PanelBcastTask = parsec::PanelBcastTask<matrix_t, scalar_t>; 
      //    // Task arguments 
      //    typename PanelBcastTask::Arguments panel_bcast_args(k, &A);
      //    // Initialize task 
      //    PanelBcastTask panel_bcast_task;
      //    panel_bcast_task.initialize(panel_bcast_args);

      //    panel_bcast_task.submit(A.parsec_high_level_tp);
      // }

      if (k+1+lookahead < A_nt) {

         // Task type
         using PotrfUpdateTrailingTask = parsec::PotrfUpdateTrailingTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfUpdateTrailingTask::Arguments update_trailing_args(k, lookahead, &A);
         // Initialize task 
         PotrfUpdateTrailingTask update_trailing_task;
         update_trailing_task.initialize(update_trailing_args);

         update_trailing_task.submit(A.parsec_high_level_tp);         
      }

      // Lookahead column(s)
      // for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) {

      //    // Task type
      //    using PotrfUpdateColumnTask = parsec::PotrfUpdateColumnTask<matrix_t, scalar_t>; 
      //    // Task arguments 
      //    typename PotrfUpdateColumnTask::Arguments update_column_args(k, j, &A);

      //    // Initialize task 
      //    PotrfUpdateColumnTask update_column_task;
      //    update_column_task.initialize(update_column_args);

      //    update_column_task.submit(A.parsec_high_level_tp);         
         
      // }

      // std::cout << "[" << context << "] myrank = " << A.mpiRank()
      //        << ", flushing data.." << std::endl;

      // for (int64_t i = k; i < A_nt; ++i) {
      //    // parsec_taskpool_t* tp = A.parsec_high_level_tp;
      //    parsec_taskpool_t *tp = A.parsec_potrf_panel_tp;
      //    auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
      //    parsec_dtd_data_flush(tp, aik_dtd_tile);
      // }

      parsec_dtd_data_flush(A.parsec_high_level_tp, A.column(k));
      // rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

   }
   
   std::cout << "[" << context << "] myrank = " << A.mpiRank()
             << ", wait for completion (parsec_high_level_tp).." << std::endl;
   rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);
   PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
   // std::cout << "[" << context << "] wait for completion (parsec_context).." << std::endl;

   rc = parsec_context_wait(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_wait");
   std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank() << ", execution done" << std::endl;
   
   A.tileUpdateAllOrigin();
   A.releaseWorkspace();

}

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::ParsecCtlFlow>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   //
   // Control-flow task dependencies, flat tasks
   // potrf_ctlflow_flat(A, lookahead);

   //
   // Control-flow task dependencies, nested tasks
   potrf_ctlflow_nested(A, lookahead);
}

template <typename scalar_t>
void potrf_dataflow(
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   std::string context = "potrf_dataflow";

   // if upper, change to lower
   if (A.uplo() == Uplo::Upper) {
      A = conj_transpose(A);
   }
   const int64_t A_nt = A.nt();
   // std::cout << "[" << context << "] my rank = " << A.mpiRank() << ", nt = " << A_nt << std::endl;

   // Debug
   // if (0 == A.mpiRank()) {
   // if (1 == A.mpiRank()) {
   //    sleep(1);
   // }

   std::chrono::time_point<std::chrono::high_resolution_clock> start, end;

   int rc; // Parsec error code
   rc = parsec_context_start(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_start");

   using matrix_t = HermitianMatrix<scalar_t>;

   start = std::chrono::high_resolution_clock::now();

   for (int64_t k = 0; k < A_nt; ++k) {
      // for (int64_t k = 0; k < 1; ++k) {

      // std::cout << "[" << context << "] k = " << k << std::endl;

      //
      // Potrf A(k,k)

      {
         // Task type
         using PotrfTask = parsec::PotrfDataTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfTask::Arguments potrf_args(k, &A);
         // Initialize task 
         PotrfTask potrf_task;
         potrf_task.initialize(potrf_args);
         
         // potrf_task.execute(); // Synchronous execution

         // Kernel call for debug purpose
         // internal::potrf<Target::HostTask>(A.sub(k, k), 1);

         // Asynchronous execution (submitted to high_level taskpool)
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         potrf_task.submit(tp, priority);
         // parsec_dtd_taskpool_wait(tp);      
      }

      //
      // Potrf Solve A(k+1:A_nt,k)

      if (k+1 <= A_nt-1) {

         for (int64_t i = k+1; i < A_nt; ++i) {

            //
            // Trsm A(i,k)
            {
               // Task type
               using PotrfSolveDataTask = parsec::PotrfSolveDataTask<matrix_t, scalar_t>; 
               // Task arguments
               typename PotrfSolveDataTask::Arguments trsm_args(i, k, &A);

               PotrfSolveDataTask trsm_task;
               trsm_task.initialize(trsm_args);

               // trsm_task.execute(); // Synchronous execution

               // Asynchronous execution (submitted to high_level taskpool)
               int priority = 2*A_nt - 2*k - i;
               trsm_task.submit(A.parsec_high_level_tp, priority);
            }

         }

         //    // auto Akk = A.sub(k, k);
         //    // auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
         //    // internal::trsm<Target::HostTask>(
         //    //       Side::Right,
         //    //       scalar_t(1.0), conj_transpose(Tkk),
         //    //       A.sub(k+1, A_nt-1, k, k), 1);

         // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);      
      }

      // 
      // Update trailing submatrix
      
      if (k+1+lookahead < A_nt) {
         // if (false) {

         // Parsec taskpool
         parsec_taskpool_t * tp = A.parsec_high_level_tp;

         //    parsec::potrf_trailing_matrix_task<matrix_t, scalar_t>(
         //          A, k, k+1+lookahead,
         //          A.column(k), //INPUT
         //          A.column(k+1+lookahead), //INOUT
         //          A.column(A.nt()-1)); //INOUT
         
         for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

            //
            // Update diagonal tile in column `j`
            
            // Task type
            using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

            UpdateDiagDataTask upd_diag_task;
            upd_diag_task.initialize(upd_diag_args);
            
            // upd_diag_task.execute(); // Synchronous execution

            int priority = 2*A_nt - 2*k - j;
            upd_diag_task.submit(tp, priority); // Synchronous execution
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            //
            // Update sub-diagonal tile in column `j`

            for (int64_t i = j+1; i < A_nt; ++i) {

               // Task type
               using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
               // Task arguments
               typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

               UpdateDataTask upd_task;
               upd_task.initialize(upd_args);

               // upd_task.execute(); // Synchronous execution
               int priority = 2*A_nt - 2*k - i - j;
               upd_task.submit(tp, priority); // Synchronous execution
  
            }
         }

         // parsec_dtd_taskpool_wait(tp);

      } // k+1+lookahead < A_nt

      // Lookahead column(s)
      for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) { // lookahead column(s)

         //
         // Update diagonal tile in column `j`
            
         // Task type
         using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

         UpdateDiagDataTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         int priority = 2*A_nt - 2*k - j;
         upd_diag_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
         // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {

            // Task type
            using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

            UpdateDataTask upd_task;
            upd_task.initialize(upd_args);

            // upd_task.execute(); // Synchronous execution
            int priority = 2*A_nt - 2*k - i - j;
            upd_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
  
         }
         
      } // Lookahead columns
      
      // Flush data
      for (int64_t i = k; i < A_nt; ++i) {

         // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;
               
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
         parsec_dtd_data_flush(tp, aik_dtd_tile);

      }

      // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

   }

   end = std::chrono::high_resolution_clock::now();

   double ttotal = std::chrono::duration_cast<std::chrono::nanoseconds>
      (end-start).count();
   ttotal = 1e-9*ttotal;

   // printf("[%s] DAG unrolling time (s) = %e\n", context, 1e-9*ttotal);

   // std::cout << "[" << context << "] myrank = "
   //           << A.mpiRank()
   //           << ", DAG unrolling time (s) = " << ttotal << std::endl;
   
   // std::cout << "[" << context << "] myrank = " << A.mpiRank() << ", wait for completion (parsec_high_level_tp).." << std::endl;
   rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);
   PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
   // std::cout << "[" << context << "] wait for completion (parsec_context).." << std::endl;

   rc = parsec_context_wait(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_wait");
   // std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank() << ", execution done" << std::endl;
   
   A.tileUpdateAllOrigin();
   A.releaseWorkspace();

}

#ifdef PARSEC_DTD_DIST_COLLECTIVES

template <typename scalar_t>
void potrf_collectives(
      HermitianMatrix<scalar_t> A, int64_t lookahead, bool trim)
{

   std::string context = "potrf_collectives";
   
   std::chrono::time_point<std::chrono::high_resolution_clock> ist_sa, ist_en;
   double t_ist = 0.0;

   // if upper, change to lower
   if (A.uplo() == Uplo::Upper) {
      A = conj_transpose(A);
   }
   const int64_t A_nt = A.nt();
   std::cout << "[" << context << "] my rank = " << A.mpiRank()
             << ", nt = " << A_nt
             << ", trim = " << trim
             << std::endl;
   
   int rc; // Parsec error code
   rc = parsec_context_start(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_start");

   using matrix_t = HermitianMatrix<scalar_t>;

   ist_sa = std::chrono::high_resolution_clock::now();

   for (int64_t k = 0; k < A_nt; ++k) {
   // for (int64_t k = 0; k < std::min((decltype(A_nt))1, A_nt); ++k) {

      // std::cout << "[" << context << "] my rank = " << A.mpiRank()
      //           << " k = " << k << std::endl;

      //
      // Potrf A(k,k)

      {
         // Task type
         using PotrfTask = parsec::PotrfDataTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfTask::Arguments potrf_args(k, &A);
         // Initialize task 
         PotrfTask potrf_task;
         potrf_task.initialize(potrf_args);
         
         // potrf_task.execute(); // Synchronous execution

         // Kernel call for debug purpose
         // internal::potrf<Target::HostTask>(A.sub(k, k), 1);

         // Asynchronous execution (submitted to high_level taskpool)
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         potrf_task.submit(tp, priority);
         // parsec_dtd_taskpool_wait(tp);      
      }

      //
      // Tile broadcast A(k,k)

      // Debug
      // if (0 == A.mpiRank()) {
      // if (0 != A.mpiRank()) {
      //    // if (2 == A.mpiRank()) {
      //    sleep(1);
      // }

      // if (k == 0 || k == 1 || k == 2 || k == 3 ) // Debug
      if (k+1 <= A_nt-1)
      // if (false)
      {
         // Task type
         using TileBcast = parsec::TileBcast<matrix_t, scalar_t>;
         // Task arguments
         typename TileBcast::Arguments bcast_args(k, k, {k+1, A_nt-1, k, k}, &A);
         // Initialize task 
         TileBcast tile_bcast;
         tile_bcast.initialize(bcast_args);

         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         tile_bcast.submit(tp, priority);
      }
      
      //
      // Potrf Solve A(k+1:A_nt,k)

      if (k+1 <= A_nt-1) {

         for (int64_t i = k+1; i < A_nt; ++i) {

            //
            // Trsm A(i,k)
            if (!trim || A.tileIsLocal(i, k))
            {
               // Task type
               using PotrfSolveDataTask = parsec::PotrfSolveDataTask<matrix_t, scalar_t>; 
               // Task arguments
               typename PotrfSolveDataTask::Arguments trsm_args(i, k, &A);

               PotrfSolveDataTask trsm_task;
               trsm_task.initialize(trsm_args);

               // trsm_task.execute(); // Synchronous execution

               // Asynchronous execution (submitted to high_level taskpool)
               int priority = 2*A_nt - 2*k - i;
               trsm_task.submit(A.parsec_high_level_tp, priority);
            }

         }

         //    // auto Akk = A.sub(k, k);
         //    // auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
         //    // internal::trsm<Target::HostTask>(
         //    //       Side::Right,
         //    //       scalar_t(1.0), conj_transpose(Tkk),
         //    //       A.sub(k+1, A_nt-1, k, k), 1);

         // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);      
      }

      // Broadcast A(i, k) across row A(i, k+1:i) and down column
      // A(i:nt-1, i)

      for (int64_t i = k+1; i < A_nt; ++i) {
      // for (int64_t i = k+1; i < std::min(k+2, A_nt); ++i) {

         // if (A.tileRank(i, k) != 0)
         //    continue;
            
         // Task type
         using TileBcast = parsec::TileBcast<matrix_t, scalar_t>;
         // Task arguments
         typename TileBcast::Arguments bcast_args(
               i, k, {{i, i, k+1, i}, {i, A_nt-1, i, i}}, &A);
               // i, k, {i, i, k+1, std::min(k+1, i)}, &A);
               // i, k, {i, i, k+1, i}, &A);
               // i, k, {i, A_nt-1, i, i}, &A);

         TileBcast tile_bcast;
         tile_bcast.initialize(bcast_args);

         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         tile_bcast.submit(tp, priority);

         // auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
         // parsec_dtd_data_flush(tp, aik_dtd_tile);
         // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

      }
      
      // 
      // Update trailing submatrix
      
      if (k+1+lookahead < A_nt) {
      // if (false) {

         // Parsec taskpool
         parsec_taskpool_t * tp = A.parsec_high_level_tp;

         //    parsec::potrf_trailing_matrix_task<matrix_t, scalar_t>(
         //          A, k, k+1+lookahead,
         //          A.column(k), //INPUT
         //          A.column(k+1+lookahead), //INOUT
         //          A.column(A.nt()-1)); //INOUT
         
         for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

            //
            // Update diagonal tile in column `j`
            
            // Task type
            using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

            UpdateDiagDataTask upd_diag_task;
            upd_diag_task.initialize(upd_diag_args);
            
            // upd_diag_task.execute(); // Synchronous execution

            int priority = 2*A_nt - 2*k - j;
            upd_diag_task.submit(tp, priority); // Synchronous execution
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            //
            // Update sub-diagonal tile in column `j`

            for (int64_t i = j+1; i < A_nt; ++i) {

               if (!trim || A.tileIsLocal(i, j))
               {
                  // Task type
                  using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
                  // Task arguments
                  typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

                  UpdateDataTask upd_task;
                  upd_task.initialize(upd_args);

                  // upd_task.execute(); // Synchronous execution
                  int priority = 2*A_nt - 2*k - i - j;
                  upd_task.submit(tp, priority); // Synchronous execution
               }
            }
         }

         // parsec_dtd_taskpool_wait(tp);

      } // k+1+lookahead < A_nt

      // Lookahead column(s)

      for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) { // lookahead column(s)

         //
         // Update diagonal tile in column `j`
            
         // Task type
         using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

         UpdateDiagDataTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         int priority = 2*A_nt - 2*k - j;
         upd_diag_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
         // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {

            // Task type
            using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

            UpdateDataTask upd_task;
            upd_task.initialize(upd_args);

            // upd_task.execute(); // Synchronous execution
            int priority = 2*A_nt - 2*k - i - j;
            upd_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
  
         }
         
      } // Lookahead columns
      
      // Flush data
      for (int64_t i = k; i < A_nt; ++i) {

         // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;
               
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
         parsec_dtd_data_flush(tp, aik_dtd_tile);

      }

      // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

   }

   // std::cout << "[" << context << "] flush all" << std::endl;

   ist_en = std::chrono::high_resolution_clock::now();

   double ttotal = std::chrono::duration_cast<std::chrono::nanoseconds>
      (ist_en-ist_sa).count();
   ttotal = 1e-9*ttotal;

   // for (int64_t k = 0; k < A_nt; ++k) {
   //    for (int64_t i = k; i < A_nt; ++i) {
   //    // for (int64_t i = 0; i < A_nt; ++i) {

   //       // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;
               
   //       parsec_taskpool_t* tp = A.parsec_high_level_tp;
   //       auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
   //       parsec_dtd_data_flush(tp, aik_dtd_tile);

   //    }
   // }

   if(A.mpiRank() == 0) {
      std::cout << "[" << context << "] myrank = "
                << A.mpiRank()
                << ", DAG unrolling time (s) = " << ttotal
                << std::endl;
   }
   
   // std::cout << "[" << context << "] myrank = " << A.mpiRank() << ", wait for completion (parsec_high_level_tp).." << std::endl;
   rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);
   PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
   // std::cout << "[" << context << "] wait for completion (parsec_context).." << std::endl;

   rc = parsec_context_wait(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_wait");
   std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank() << ", execution done" << std::endl;
   
   A.tileUpdateAllOrigin();
   A.releaseWorkspace();  
}

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::ParsecCollectives>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   //
   // dataflow task dependencies including collective communications
   potrf_collectives(A, lookahead, false);
}

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::ParsecCollecTrim>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   //
   // dataflow task dependencies including collective communications
   potrf_collectives(A, lookahead, true);
}

template <typename scalar_t>
void potrf_batch(
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   std::string context = "potrf_batch";

   // Timings
   std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
   std::chrono::time_point<std::chrono::high_resolution_clock> upd_sa, upd_en;
   double t_upd = 0.0;
   std::chrono::time_point<std::chrono::high_resolution_clock> ist_upd_sa, ist_upd_en;
   double t_ist_upd = 0.0;
   std::chrono::time_point<std::chrono::high_resolution_clock> crt_upd_sa, crt_upd_en;
   double t_crt_upd = 0.0;
   int64_t num_upd_task = 0;
   
   // if upper, change to lower
   if (A.uplo() == Uplo::Upper) {
      A = conj_transpose(A);
   }
   const int64_t A_nt = A.nt();
   std::cout << "[" << context << "] my rank = " << A.mpiRank() << ", nt = " << A_nt << std::endl;
   
   int rc; // Parsec error code
   rc = parsec_context_start(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_start");

   using matrix_t = HermitianMatrix<scalar_t>;

   matrix_t *pA = &A;

   start = std::chrono::high_resolution_clock::now();

   for (int64_t k = 0; k < A_nt; ++k) {
   // for (int64_t k = 0; k < std::min((decltype(A_nt))1, A_nt); ++k) {

      // std::cout << "[" << context << "] my rank = " << A.mpiRank()
      //           << " k = " << k << std::endl;

      //
      // Potrf A(k,k)

      {
         // Task type
         using PotrfTask = parsec::PotrfDataTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfTask::Arguments potrf_args(k, &A);
         // Initialize task 
         PotrfTask potrf_task;
         potrf_task.initialize(potrf_args);
         
         // potrf_task.execute(); // Synchronous execution

         // Kernel call for debug purpose
         // internal::potrf<Target::HostTask>(A.sub(k, k), 1);

         // Asynchronous execution (submitted to high_level taskpool)
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         potrf_task.submit(tp, priority);
         // parsec_dtd_taskpool_wait(tp);      
      }

      //
      // Tile broadcast A(k,k)

      // Debug
      // if (0 == A.mpiRank()) {
      // if (0 != A.mpiRank()) {
      //    // if (2 == A.mpiRank()) {
      //    sleep(1);
      // }

      // if (k == 0 || k == 1 || k == 2 || k == 3 ) // Debug
      if (k+1 <= A_nt-1)
      // if (false)
      {
         // Task type
         using TileBcast = parsec::TileBcast<matrix_t, scalar_t>;
         // Task arguments
         typename TileBcast::Arguments bcast_args(k, k, {k+1, A_nt-1, k, k}, &A);
         // Initialize task 
         TileBcast tile_bcast;
         tile_bcast.initialize(bcast_args);

         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         tile_bcast.submit(tp, priority);
      }
      
      //
      // Potrf Solve A(k+1:A_nt,k)

      if (k+1 <= A_nt-1) {

         for (int64_t i = k+1; i < A_nt; ++i) {

            //
            // Trsm A(i,k)
            {
               // Task type
               using PotrfSolveDataTask = parsec::PotrfSolveDataTask<matrix_t, scalar_t>; 
               // Task arguments
               typename PotrfSolveDataTask::Arguments trsm_args(i, k, &A);

               PotrfSolveDataTask trsm_task;
               trsm_task.initialize(trsm_args);

               // trsm_task.execute(); // Synchronous execution

               // Asynchronous execution (submitted to high_level taskpool)
               int priority = 2*A_nt - 2*k - i;
               trsm_task.submit(A.parsec_high_level_tp, priority);
            }

         }

         //    // auto Akk = A.sub(k, k);
         //    // auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
         //    // internal::trsm<Target::HostTask>(
         //    //       Side::Right,
         //    //       scalar_t(1.0), conj_transpose(Tkk),
         //    //       A.sub(k+1, A_nt-1, k, k), 1);

         // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);      
      }

      // Broadcast A(i, k) across row A(i, k+1:i) and down column
      // A(i:nt-1, i)

      for (int64_t i = k+1; i < A_nt; ++i) {
      // for (int64_t i = k+1; i < std::min(k+2, A_nt); ++i) {

         // if (A.tileRank(i, k) != 0)
         //    continue;
            
         // Task type
         using TileBcast = parsec::TileBcast<matrix_t, scalar_t>;
         // Task arguments
         typename TileBcast::Arguments bcast_args(
               i, k, {{i, i, k+1, i}, {i, A_nt-1, i, i}}, &A);
               // i, k, {i, i, k+1, std::min(k+1, i)}, &A);
               // i, k, {i, i, k+1, i}, &A);
               // i, k, {i, A_nt-1, i, i}, &A);

         TileBcast tile_bcast;
         tile_bcast.initialize(bcast_args);

         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         tile_bcast.submit(tp, priority);

         // auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
         // parsec_dtd_data_flush(tp, aik_dtd_tile);
         // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

      }
      
      // 
      // Update trailing submatrix
      
      if (k+1+lookahead < A_nt) {
      // if (false) {

         // Parsec taskpool
         parsec_taskpool_t * tp = A.parsec_high_level_tp;

         //    parsec::potrf_trailing_matrix_task<matrix_t, scalar_t>(
         //          A, k, k+1+lookahead,
         //          A.column(k), //INPUT
         //          A.column(k+1+lookahead), //INOUT
         //          A.column(A.nt()-1)); //INOUT
         
         // using PotrfUpdateType = parsec::PotrfUpdate<matrix_t, scalar_t>; 

         // typename PotrfUpdateType::Arguments upd_args(k, lookahead, &A);

         // PotrfUpdateType upd;
         // upd.initialize(upd_args);

         // int priority = 2*A_nt - 2*k;
         // upd.submit(tp, priority); // Synchronous execution

         for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

            //
            // Update diagonal tile in column `j`
            
            // Task type
            using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

            UpdateDiagDataTask upd_diag_task;
            upd_diag_task.initialize(upd_diag_args);
            
            // upd_diag_task.execute(); // Synchronous execution

            int priority = 2*A_nt - 2*k - j;
            upd_diag_task.submit(tp, priority); // Synchronous execution
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            //
            // Update sub-diagonal tile in column `j`

            for (int64_t i = j+1; i < A_nt; ++i) {

               // // Task type
               // using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
               // // Task arguments
               // typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

               // UpdateDataTask upd_task;
               // upd_task.initialize(upd_args);

               // // upd_task.execute(); // Synchronous execution
               int priority = 2*A_nt - 2*k - i - j;
               // upd_task.submit(tp, priority); // Synchronous execution
               

               // Retrieve rank owning tile a(i, j)
               int tile_rank = A.tileRank(i, j);

               // A(i, k) tile
               auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
               int aik_arena_index = (A.tileRank(i, k) == A.mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

               // A(j, k) tile
               auto* ajk_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), j, k)];
               int ajk_arena_index = (A.tileRank(j, k) == A.mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

               // A(i, j) tile
               auto* aij_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, j)];
               int aij_arena_index = (A.tileRank(i, j) == A.mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

               // upd_sa = std::chrono::high_resolution_clock::now();

               // crt_upd_sa = std::chrono::high_resolution_clock::now();
               parsec_task_t *parsec_upd_task = parsec_dtd_taskpool_create_task(
                     tp,
                     parsec::PotrfUpdateDataTask<matrix_t, scalar_t>::parsec_cpu_func,
                     priority, /* inherits the priority from high level task */
                     "PotrfUpdateTask",
                     sizeof(decltype(k)), &k, VALUE,
                     sizeof(decltype(j)), &i, VALUE,
                     sizeof(decltype(j)), &j, VALUE,
                     sizeof(matrix_t *), &pA, VALUE,
                     PASSED_BY_REF, aik_dtd_tile, INPUT | aik_arena_index /*| AFFINITY*/,
                     PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index /*| AFFINITY*/,
                     PASSED_BY_REF, aij_dtd_tile, INOUT | aij_arena_index /*| AFFINITY*/,
                     sizeof(int), &tile_rank, VALUE | AFFINITY,
                     PARSEC_DTD_ARG_END);

               crt_upd_sa = std::chrono::high_resolution_clock::now();
               parsec_dtd_task_is_local((parsec_dtd_task_t *)parsec_upd_task);
               crt_upd_en = std::chrono::high_resolution_clock::now();
               t_crt_upd += std::chrono::duration_cast<std::chrono::nanoseconds>
                  (crt_upd_en-crt_upd_sa).count();

               ist_upd_sa = std::chrono::high_resolution_clock::now();
               parsec_insert_dtd_task(parsec_upd_task);
               ist_upd_en = std::chrono::high_resolution_clock::now();

               t_ist_upd += std::chrono::duration_cast<std::chrono::nanoseconds>
                  (ist_upd_en-ist_upd_sa).count();
               // t_upd += std::chrono::duration_cast<std::chrono::nanoseconds>(upd_en-upd_sa).count();

               ++num_upd_task;
            }
         }

         // parsec_dtd_taskpool_wait(tp);

      } // k+1+lookahead < A_nt

         
      // Lookahead column(s)

      for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) { // lookahead column(s)

         //
         // Update diagonal tile in column `j`
            
         // Task type
         using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

         UpdateDiagDataTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         int priority = 2*A_nt - 2*k - j;
         upd_diag_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
         // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {

            // Task type
            using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

            UpdateDataTask upd_task;
            upd_task.initialize(upd_args);

            // upd_task.execute(); // Synchronous execution
            int priority = 2*A_nt - 2*k - i - j;
            upd_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
  
         }
         
      } // Lookahead columns
      
      // Flush data
      for (int64_t i = k; i < A_nt; ++i) {

         // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;
               
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
         parsec_dtd_data_flush(tp, aik_dtd_tile);

      }

      // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

   } // Loop over `k`

   end = std::chrono::high_resolution_clock::now();

   double ttotal = std::chrono::duration_cast<std::chrono::nanoseconds>
      (end-start).count();
   ttotal = 1e-9*ttotal;

   // printf("[%s] DAG unrolling time (s) = %e\n", context, 1e-9*ttotal);

   std::cout << "[" << context << "] myrank = "
             << A.mpiRank()
             << ", DAG unrolling time (s) = " << ttotal
             << ", upd insert (s) = " << 1e-9*t_ist_upd
             << ", num upd = " << num_upd_task
             << ", upd insert / per task (s) = " << (1e-9*t_ist_upd) / ((double)num_upd_task)
             << ", upd create  (s) = " << 1e-9*t_crt_upd
             << ", upd create / per task (s) = " << (1e-9*t_crt_upd) / ((double)num_upd_task)
             << std::endl;

   // std::cout << "[" << context << "] flush all" << std::endl;

   // for (int64_t k = 0; k < A_nt; ++k) {
   //    for (int64_t i = k; i < A_nt; ++i) {
   //    // for (int64_t i = 0; i < A_nt; ++i) {

   //       // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;
               
   //       parsec_taskpool_t* tp = A.parsec_high_level_tp;
   //       auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
   //       parsec_dtd_data_flush(tp, aik_dtd_tile);

   //    }
   // }
   
   // std::cout << "[" << context << "] myrank = " << A.mpiRank() << ", wait for completion (parsec_high_level_tp).." << std::endl;
   rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);
   PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
   // std::cout << "[" << context << "] wait for completion (parsec_context).." << std::endl;

   rc = parsec_context_wait(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_wait");
   std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank() << ", execution done" << std::endl;
   
   A.tileUpdateAllOrigin();
   A.releaseWorkspace();  
}

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::ParsecBatch>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   //
   // dataflow task dependencies including collective communications
   potrf_batch(A, lookahead);
}

#endif /* PARSEC_DTD_DIST_COLLECTIVES */

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::Parsec>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{
      
   //
   // data-flow task dependencies, flat DAG
   potrf_dataflow(A, lookahead);
}

#ifdef PARSEC_DTD_HAVE_CUDA

template <typename scalar_t>
void potrf_device(
      HermitianMatrix<scalar_t> A, int64_t lookahead, bool panel_on_gpu)
{

   std::string context = "potrf_device";
   
   // if upper, change to lower
   if (A.uplo() == Uplo::Upper) {
      A = conj_transpose(A);
   }
   const int64_t A_nt = A.nt();

   // parsec::CuHI = parsec_info_register(&parsec_per_device_infos, "CUBLAS::HANDLE", NULL);
   // assert(parsec::CuHI != -1);

   // TODO: Retrieve the number of device per context 
   int nb_cuda_devices = parsec::get_nb_cuda_devices();
   std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank()
             << ", nb_cuda_devices = " << nb_cuda_devices
             << std::endl;

   std::vector<int> cuda_dev_index = parsec::get_cuda_dev_index();

   std::cout << "[" << context << "] my rank = " <<  A.mpiRank()
             << ", dev_index = ";  
   for (int dev_index: cuda_dev_index) {
      std::cout << dev_index << "; ";
   }
   std::cout << std::endl;
   
   // std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank()
   //           << ", num_devices = " << A.num_devices()
   //           << std::endl;

   int rc; // Parsec error code
   rc = parsec_context_start(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_start");

   using matrix_t = HermitianMatrix<scalar_t>;

   for (int64_t k = 0; k < A_nt; ++k) {
   // for (int64_t k = 0; k < 2; ++k) {

      // std::cout << "[" << context << "] k = " << k << std::endl;

      //
      // Potrf A(k,k)

      {
         // Task type
         using PotrfTask = parsec::PotrfDataTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfTask::Arguments potrf_args(k, &A);
         if (k > 0) {
            // Pullin Data from GPU for column k > 0
            potrf_args.pullin = true;
         }
         // Initialize task 
         PotrfTask potrf_task;
         potrf_task.initialize(potrf_args);

         
         // potrf_task.execute(); // Synchronous execution

         // Kernel call for debug purpose
         // internal::potrf<Target::HostTask>(A.sub(k, k), 1);

         // Asynchronous execution (submitted to high_level taskpool)
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         int priority = 2*A_nt - 2*k;
         potrf_task.submit(tp, priority);
         // parsec_dtd_taskpool_wait(tp);      
      }

      //
      // Potrf Solve A(k+1:A_nt,k)

      if (k+1 <= A_nt-1) {

         for (int64_t i = k+1; i < A_nt; ++i) {

            //
            // Trsm A(i,k)
            if (panel_on_gpu) {
               
               // Task type
               // using PotrfSolveDataTask = parsec::PotrfSolveDataTask<matrix_t, scalar_t>; 
               using PotrfSolveDataTask = parsec::PotrfSolveGpuTask<matrix_t, scalar_t>; 
               // Task arguments
               typename PotrfSolveDataTask::Arguments trsm_args(i, k, &A);

               // if (k > 0) {
               //    // Pullin Data from GPU for column k > 0
               //    trsm_args.pullin = true;
               // }
               trsm_args.pushout = true;

               PotrfSolveDataTask trsm_task;
               trsm_task.initialize(trsm_args);

               // trsm_task.execute(); // Synchronous execution

               // Asynchronous execution (submitted to high_level taskpool)
               int priority = 2*A_nt - 2*k - i;
               trsm_task.submit(A.parsec_high_level_tp, priority);
            }
            else {
               // Task type
               using PotrfSolveDataTask = parsec::PotrfSolveDataTask<matrix_t, scalar_t>; 
               // Task arguments
               typename PotrfSolveDataTask::Arguments trsm_args(i, k, &A);

               if (k > 0) {
                  // Pullin Data from GPU for column k > 0
                  trsm_args.pullin = true;
               }

               PotrfSolveDataTask trsm_task;
               trsm_task.initialize(trsm_args);

               // trsm_task.execute(); // Synchronous execution

               // Asynchronous execution (submitted to high_level taskpool)
               int priority = 2*A_nt - 2*k - i;
               trsm_task.submit(A.parsec_high_level_tp, priority);
            }

         }

         //    // auto Akk = A.sub(k, k);
         //    // auto Tkk = TriangularMatrix< scalar_t >(Diag::NonUnit, Akk);
         //    // internal::trsm<Target::HostTask>(
         //    //       Side::Right,
         //    //       scalar_t(1.0), conj_transpose(Tkk),
         //    //       A.sub(k+1, A_nt-1, k, k), 1);

         // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);      
      }

      // 
      // Update trailing submatrix
      
      if (k+1+lookahead < A_nt) {
         // if (false) {

         // Parsec taskpool
         parsec_taskpool_t * tp = A.parsec_high_level_tp;

         //    parsec::potrf_trailing_matrix_task<matrix_t, scalar_t>(
         //          A, k, k+1+lookahead,
         //          A.column(k), //INPUT
         //          A.column(k+1+lookahead), //INOUT
         //          A.column(A.nt()-1)); //INOUT
         
         for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

            //
            // Update diagonal tile in column `j`
            
            // Task type

            // if (k == 0 && j == 2/*true*/) { // DEBUG

            // using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; // DEBUG
            using UpdateDiagDataTask = parsec::PotrfUpdateDiagGpuTask<matrix_t, scalar_t>;

            // Task arguments
            typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);
            if (k+1 == j) {
               upd_diag_args.pushout = true;
            }
            UpdateDiagDataTask upd_diag_task;
            upd_diag_task.initialize(upd_diag_args);
            
            // upd_diag_task.execute(); // Synchronous execution

            int priority = 2*A_nt - 2*k - j;
            upd_diag_task.submit(tp, priority); // Synchronous execution
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);      
            // }
            // else {

            //    using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; // DEBUG
            //    // using UpdateDiagDataTask = parsec::PotrfUpdateDiagGpuTask<matrix_t, scalar_t>;
               
            //    // Task arguments
            //    typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

            //    UpdateDiagDataTask upd_diag_task;
            //    upd_diag_task.initialize(upd_diag_args);
            
            //    // upd_diag_task.execute(); // Synchronous execution

            //    int priority = 2*A_nt - 2*k - j;
            //    upd_diag_task.submit(tp, priority); // Synchronous execution
            //    // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            // }
               
            //
            // Update sub-diagonal tile in column `j`

            for (int64_t i = j+1; i < A_nt; ++i) {

               // Task type
               // using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; // DEBUG
               using UpdateDataTask = parsec::PotrfUpdateGpuTask<matrix_t, scalar_t>; 
               // using UpdateDataTask = parsec::PotrfUpdateTCTask<matrix_t, scalar_t>; 

               // Task arguments
               typename UpdateDataTask::Arguments upd_args(k, i, j, &A);
               if (!panel_on_gpu && (k+1 == j)) {
                  upd_args.pushout = true;
               }

               UpdateDataTask upd_task;
               upd_task.initialize(upd_args);

               // upd_task.execute(); // Synchronous execution
               int priority = 2*A_nt - 2*k - i - j;
               upd_task.submit(tp, priority); // Synchronous execution
  
            }
         }

         // parsec_dtd_taskpool_wait(tp);

      } // k+1+lookahead < A_nt

      // Lookahead column(s)
      for (int64_t j = k+1; j < k+1+lookahead && j < A_nt; ++j) { // lookahead column(s)

         //
         // Update diagonal tile in column `j`
            
         // Task type
         // using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
         using UpdateDiagDataTask = parsec::PotrfUpdateDiagGpuTask<matrix_t, scalar_t>;
         // Task arguments
         typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, &A);

         UpdateDiagDataTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         int priority = 2*A_nt - 2*k - j;
         upd_diag_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
         // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {

            // Task type
            using UpdateDataTask = parsec::PotrfUpdateGpuTask<matrix_t, scalar_t>; 
            // using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateDataTask::Arguments upd_args(k, i, j, &A);

            UpdateDataTask upd_task;
            upd_task.initialize(upd_args);

            // upd_task.execute(); // Synchronous execution
            int priority = 2*A_nt - 2*k - i - j;
            upd_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
  
         }
         
      } // Lookahead columns
      
      // Flush data
      for (int64_t i = k; i < A_nt; ++i) {

         // if (i > k) {
         //    // Task type
         //    using PullinTask = parsec::PullinDataTask<matrix_t, scalar_t>; 
         //    // Task arguments
         //    typename PullinTask::Arguments pullin_args(i, k, &A);
         //    PullinTask pullin_task;
         //    pullin_task.initialize(pullin_args);
         //    int priority = 2*A_nt;
         //    pullin_task.submit(A.parsec_high_level_tp, priority); // Asynchronous execution
         // }
         
         // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;               
         parsec_taskpool_t* tp = A.parsec_high_level_tp;
         auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
         parsec_dtd_data_flush(tp, aik_dtd_tile);
      }

      // parsec_dtd_taskpool_wait(A.parsec_high_level_tp);

   }

   // Debug
   // for (int64_t k = 0; k < A_nt; ++k) {
   //    for (int64_t i = k; i < A_nt; ++i) {
   //    // for (int64_t i = 0; i < A_nt; ++i) {
   //       // std::cout << "[" << context << "] flush A (" << i << ", " << k << ")" << std::endl;               
   //       parsec_taskpool_t* tp = A.parsec_high_level_tp;
   //       auto* aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
   //       parsec_dtd_data_flush(tp, aik_dtd_tile);
   //    }
   // }

   
   std::cout << "[" << context << "] myrank = " << A.mpiRank() << ", wait for completion (parsec_high_level_tp).." << std::endl;
   rc = parsec_dtd_taskpool_wait(A.parsec_high_level_tp);
   PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
   // std::cout << "[" << context << "] wait for completion (parsec_context).." << std::endl;

   rc = parsec_context_wait(A.parsec_context);
   PARSEC_CHECK_ERROR(rc, "parsec_context_wait");
   // std::cout << "[potrf Target::Parsec] my rank = " <<  A.mpiRank() << ", execution done" << std::endl;

   // parsec_info_unregister(&parsec_per_device_infos, parsec::CuHI, NULL);

   A.tileUpdateAllOrigin();
   A.releaseWorkspace();

}

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::ParsecDevice>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   //
   // Dataflow task dependencies with CUDA 
   potrf_device(A, lookahead, false);
}

template <typename scalar_t>
void potrf(
      slate::internal::TargetType<Target::ParsecDevPanel>,
      HermitianMatrix<scalar_t> A, int64_t lookahead)
{

   //
   // Dataflow task dependencies using CUDA including panel
   potrf_device(A, lookahead, true);
}

#endif

#endif

} // namespace specialization
} // namespace internal

//------------------------------------------------------------------------------
/// Version with target as template parameter.
/// @ingroup posv_specialization
///
template <Target target, typename scalar_t>
void potrf(HermitianMatrix<scalar_t>& A,
           Options const& opts)
{
    int64_t lookahead;
    try {
        lookahead = opts.at(Option::Lookahead).i_;
        assert(lookahead >= 0);
    }
    catch (std::out_of_range&) {
        lookahead = 1;
    }

    internal::specialization::potrf(internal::TargetType<target>(),
                                    A, lookahead);
}

//------------------------------------------------------------------------------
/// Distributed parallel Cholesky factorization.
///
/// Performs the Cholesky factorization of a Hermitian positive definite
/// matrix $A$.
///
/// The factorization has the form
/// \[
///     A = L L^H,
/// \]
/// if $A$ is stored lower, where $L$ is a lower triangular matrix, or
/// \[
///     A = U^H U,
/// \]
/// if $A$ is stored upper, where $U$ is an upper triangular matrix.
///
//------------------------------------------------------------------------------
/// @tparam scalar_t
///     One of float, double, std::complex<float>, std::complex<double>.
//------------------------------------------------------------------------------
/// @param[in,out] A
///     On entry, the n-by-n Hermitian positive definite matrix $A$.
///     On exit, if return value = 0, the factor $U$ or $L$ from the Cholesky
///     factorization $A = U^H U$ or $A = L L^H$.
///     If scalar_t is real, $A$ can be a SymmetricMatrix object.
///
/// @param[in] opts
///     Additional options, as map of name = value pairs. Possible options:
///     - Option::Lookahead:
///       Number of panels to overlap with matrix updates.
///       lookahead >= 0. Default 1.
///     - Option::Target:
///       Implementation to target. Possible values:
///       - HostTask:  OpenMP tasks on CPU host [default].
///       - HostNest:  nested OpenMP parallel for loop on CPU host.
///       - HostBatch: batched BLAS on CPU host.
///       - Devices:   batched BLAS on GPU device.
///
/// TODO: return value
/// @retval 0 successful exit
/// @retval >0 for return value = $i$, the leading minor of order $i$ of $A$ is not
///         positive definite, so the factorization could not
///         be completed, and the solution has not been computed.
///
/// @ingroup posv_computational
///
template <typename scalar_t>
void potrf(HermitianMatrix<scalar_t>& A,
           Options const& opts)
{
    Target target;
    try {
        target = Target(opts.at(Option::Target).i_);
    }
    catch (std::out_of_range&) {
        target = Target::HostTask;
    }

    switch (target) {
        case Target::Host:
        case Target::HostTask:
            potrf<Target::HostTask>(A, opts);
            // potrf<Target::Parsec>(A, opts);
            break;
        case Target::HostNest:
            potrf<Target::HostNest>(A, opts);
            break;
        case Target::HostBatch:
            potrf<Target::HostBatch>(A, opts);
            break;
        case Target::Devices:
            potrf<Target::Devices>(A, opts);
            break;
#if defined SLATE_WITH_PARSEC
        case Target::Parsec:
            potrf<Target::Parsec>(A, opts);
            break;
        case Target::ParsecCtlFlow:
            potrf<Target::ParsecCtlFlow>(A, opts);
            break;
#if defined PARSEC_DTD_DIST_COLLECTIVES
        case Target::ParsecCollectives:
            potrf<Target::ParsecCollectives>(A, opts);
            break;
        case Target::ParsecCollecTrim:
            potrf<Target::ParsecCollecTrim>(A, opts);
            break;
        case Target::ParsecBatch:
            potrf<Target::ParsecBatch>(A, opts);
            break;
#endif
#ifdef PARSEC_DTD_HAVE_CUDA
        case Target::ParsecDevice:
           potrf<Target::ParsecDevice>(A, opts);
           break;
        case Target::ParsecDevPanel:
           potrf<Target::ParsecDevPanel>(A, opts);
           break;
#endif
#endif            
    }
    // todo: return value for errors?
}

//------------------------------------------------------------------------------
// Explicit instantiations.
template
void potrf<float>(
    HermitianMatrix<float>& A,
    Options const& opts);

template
void potrf<double>(
    HermitianMatrix<double>& A,
    Options const& opts);

template
void potrf< std::complex<float> >(
    HermitianMatrix< std::complex<float> >& A,
    Options const& opts);

template
void potrf< std::complex<double> >(
    HermitianMatrix< std::complex<double> >& A,
    Options const& opts);

} // namespace slate
