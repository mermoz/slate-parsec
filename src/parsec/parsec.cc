#include "slate/parsec/parsec.hh"

#include <map>
#include <vector>

namespace parsec {

#if defined(PARSEC_HAVE_CUDA)

std::map<cudaStream_t, cublasHandle_t> cublas_handles;

cublasHandle_t get_cublas_handle(cudaStream_t custream) {

   cublasStatus_t status;
   cublasHandle_t cuhandle = NULL;

   auto cuhandle_it = parsec::cublas_handles.find(custream);
   if (cuhandle_it == parsec::cublas_handles.end()) {

      status = cublasCreate(&cuhandle);
      status = cublasSetStream(cuhandle, custream);
      parsec::cublas_handles.insert({custream, cuhandle});
            
   }
   else {
      cuhandle = cuhandle_it->second;
   }

   return cuhandle;
}
   
// parsec_info_id_t CuHI;
   
// cublasHandle_t create_or_lookup_cublas_handle(parsec_device_module_t *mod,
//                                               parsec_info_id_t cuhi,
//                                               cudaStream_t cuda_stream)
// {
//    cublasHandle_t ret;
//    ret = (cublasHandle_t)parsec_info_get(&mod->infos, cuhi);
//    if( NULL == ret ) {
//       cublasHandle_t handle;
//       cublasStatus_t status;
//       /* No need to call cudaSetDevice, as this has been done by PaRSEC before calling the task body */
//       status = cublasCreate(&handle);
//       assert(CUBLAS_STATUS_SUCCESS == status);
//       status = cublasSetStream(handle, cuda_stream);
//       assert(CUBLAS_STATUS_SUCCESS == status);
//       ret = (cublasHandle_t)parsec_info_test_and_set(&mod->infos, cuhi, handle, NULL);
//       /* We are the only thread modifying this entry, so the test and set should always succeed */
//       assert(ret == handle);
//    }
//    return ret;
// }

int get_nb_cuda_devices() {

   int nb = 0;

   for (int dev = 0; dev < (int)parsec_nb_devices; dev++) {
      parsec_device_module_t *device = parsec_mca_device_get(dev);
      if ( PARSEC_DEV_CUDA == device->type ) {
         nb++;
      }
   }
   
   return nb;
}

std::vector<int> get_cuda_dev_index() {

   std::vector<int> dev_index;
   
   for (int dev = 0; dev < (int)parsec_nb_devices; dev++) {
      parsec_device_module_t *device = parsec_mca_device_get(dev);
      if ( PARSEC_DEV_CUDA == device->type ) {
         dev_index.push_back(device->device_index);
      }
   }

   return dev_index;
}
   
#endif

}
