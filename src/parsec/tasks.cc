#include "slate/parsec/tasks.hh"


namespace parsec {

   int dummy_kernel(parsec_execution_stream_t *es, parsec_task_t *this_task) {
      (void)es;

      std::cout << "[dummy]" << std::endl;

      // usleep(1000);
      
      return PARSEC_HOOK_RETURN_DONE;
      // return PARSEC_HOOK_RETURN_ASYNC;
      // return PARSEC_HOOK_RETURN_AGAIN;
      // return PARSEC_HOOK_RETURN_ERROR;
   }

   
   void dummy_task(parsec_taskpool_t  *tp) {
      
      std::cout << "[dummy_task]" << std::endl;

      parsec_dtd_taskpool_insert_task(
            tp,
            dummy_kernel,
            0, /* Priority */
            "dummy",
            // We create the dependency to wait for the communication
            PARSEC_DTD_ARG_END);

   }

}
