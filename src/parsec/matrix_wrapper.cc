
#include "slate/parsec/matrix_wrapper.hh"

namespace parsec {

   parsec_info_id_t CuHI;

   int slate_parsec_complete_tp_callback(parsec_taskpool_t* tp, void* cb_data) {
      // std::cout << "[slate_parsec_complete_tp_callback]" << std::endl;

      parsec_taskpool_set_complete_callback(tp,  NULL, NULL);
      int rc = PARSEC_HOOK_RETURN_DONE;
      parsec_task_t *this_task = (parsec_task_t*)cb_data;
      parsec_execution_stream_t *es = this_task->taskpool->context->virtual_processes[0]->execution_streams[0];
      rc = __parsec_complete_execution(es, this_task);

      /* lets dequeue the taskpool of parent task */
      parsec_dtd_dequeue_taskpool(this_task->taskpool);

      return rc;
   }

   // int slate_parsec_complete_lookahead_tp_callback(parsec_taskpool_t* tp, void* cb_data)
   // {
   //     parsec_taskpool_set_complete_callback(tp,  NULL, NULL);
   //     int rc = PARSEC_HOOK_RETURN_DONE;

   //     if (1 == fetch_sub(,1)) { // last guy to complete
   //         parsec_task_t *this_task = (parsec_task_t*)cb_data;
   //         rc = __parsec_complete_execution(this_task->taskpool->context->virtual_processes[0]->execution_streams[0],
   //                                          this_task);
   //     }
   //     return rc;
   // }

   static uint32_t       parsec_column_rank_of(parsec_data_collection_t* dc, ...);
   static int32_t        parsec_column_vpid_of(parsec_data_collection_t* dc, ...);
   static parsec_data_t* parsec_column_data_of(parsec_data_collection_t* dc, ...);
   static uint32_t       parsec_column_rank_of_key(parsec_data_collection_t* dc, parsec_data_key_t key);
   static int32_t        parsec_column_vpid_of_key(parsec_data_collection_t* dc, parsec_data_key_t key);
   static parsec_data_t* parsec_column_data_of_key(parsec_data_collection_t* dc, parsec_data_key_t key);
   static parsec_key_t   parsec_column_data_key(parsec_data_collection_t *dc, ...);
   // static void           parsec_column_key_to_coordinates(parsec_data_collection_t *desc, parsec_data_key_t key, int64_t *i, int64_t *j);
   static void           parsec_column_key_to_coordinates(parsec_data_collection_t *dc, parsec_data_key_t key, uint32_t *r);

    uint32_t
    parsec_column_rank_of(parsec_data_collection_t* dc, ...)
    {
        return (uint32_t) dc->myrank;
    }

    int32_t
    parsec_column_vpid_of(parsec_data_collection_t* dc, ...)
    {
        return 0;
    }

    parsec_data_t *
    parsec_column_data_of(parsec_data_collection_t* dc, ...)
    {
        uint32_t r;
        va_list ap;
        parsec_column_t * desc = (parsec_column_t *)dc;

        va_start(ap, dc);
        r = va_arg(ap, uint32_t);
        va_end(ap);

        return desc->data[r];
    }

    parsec_key_t
    parsec_column_data_key(parsec_data_collection_t *dc, ...)
    {
        uint32_t r;
        va_list ap;

        va_start(ap, dc);
        r = va_arg(ap, uint32_t);
        va_end(ap);

        return r;
    }

    void
    parsec_column_key_to_coordinates(parsec_data_collection_t *dc,
                                            parsec_data_key_t key, uint32_t *r)
    {
        *r = key;
    }

    uint32_t
    parsec_column_rank_of_key(parsec_data_collection_t* dc, parsec_data_key_t key)
    {
        return (uint32_t) dc->myrank;
    }

    int32_t
    parsec_column_vpid_of_key(parsec_data_collection_t* dc, parsec_data_key_t key)
    {
        uint32_t r;
        parsec_column_key_to_coordinates(dc, key, &r);
        return parsec_column_vpid_of(dc, r);
    }

    parsec_data_t *
    parsec_column_data_of_key(parsec_data_collection_t* dc, parsec_data_key_t key)
    {
        uint32_t r;
        parsec_column_key_to_coordinates(dc, key, &r);
        return parsec_column_data_of(dc, r);
    }

    parsec_data_t*
    column_data_create(parsec_column_t *desc,
                       uint32_t key, void *ptr)
    {
        parsec_data_t *data = NULL;

        data = PARSEC_OBJ_NEW(parsec_data_t);
        data->owner_device = 0;
        data->key = key;
        data->dc = (parsec_data_collection_t*)desc;
        data->nb_elts = sizeof(int32_t);

        parsec_data_copy_t* data_copy = (parsec_data_copy_t*)PARSEC_OBJ_NEW(parsec_data_copy_t);
        parsec_data_copy_attach(data, data_copy, 0);
        data_copy->device_private = ptr;
        return data;
    }

    void parsec_column_init(parsec_column_t **holder, int64_t nt, int my_rank)
    {
        parsec_column_t *desc = (parsec_column_t*)calloc(1, sizeof(parsec_column_t));

        parsec_data_collection_t *o = (parsec_data_collection_t*)desc;
        parsec_data_collection_init( o, 1, my_rank );
        parsec_dtd_data_collection_init(o);

        o->rank_of      = parsec_column_rank_of;
        o->vpid_of      = parsec_column_vpid_of;
        o->data_of      = parsec_column_data_of;
        o->rank_of_key  = parsec_column_rank_of_key;
        o->vpid_of_key  = parsec_column_vpid_of_key;
        o->data_of_key  = parsec_column_data_of_key;
        o->data_key     = parsec_column_data_key;

        desc->nt = nt;
        desc->data = new parsec_data_t*[nt];
        desc->array = new int32_t[nt];

        for (int64_t n = 0; n < nt; ++n) {
            desc->array[n] = 42;
            desc->data[n] = column_data_create(desc, o->data_key(o, n), desc->array+n);
        }

        *holder = desc;
    }

    void parsec_column_fini( parsec_column_t **desc)
    {

       assert(NULL != *desc);
          
       parsec_dtd_data_collection_fini((parsec_data_collection_t*) *desc);
        delete [] (*desc)->array;
        for (int64_t n = 0; n < (*desc)->nt; ++n)
            parsec_data_destroy((*desc)->data[n]);
        delete [] (*desc)->data;

        *desc = NULL;
    }

    uint32_t
    parsec_two_dim_block_rank_of(parsec_data_collection_t* dc, ...)
    {
        int64_t i, j;
        va_list ap;
        parsec_two_dim_block_t * desc = (parsec_two_dim_block_t *)dc;

        va_start(ap, dc);
        i = va_arg(ap, int64_t);
        j = va_arg(ap, int64_t);
        va_end(ap);

        return (uint32_t)  i%desc->p + (j%desc->q)*desc->p;
    }

    int32_t
    parsec_two_dim_block_vpid_of(parsec_data_collection_t* dc, ...)
    {
        return 0;
    }

    parsec_data_t *
    parsec_two_dim_block_data_of(parsec_data_collection_t* dc, ...)
    {
        int64_t i, j;
        va_list ap;
        parsec_two_dim_block_t * desc = (parsec_two_dim_block_t *)dc;

        va_start(ap, dc);
        i = va_arg(ap, int64_t);
        j = va_arg(ap, int64_t);
        va_end(ap);

        return desc->data_map[i*desc->nt+j];
    }

    parsec_key_t
    parsec_two_dim_block_data_key(parsec_data_collection_t *dc, ...)
    {
        int64_t i, j;
        va_list ap;
        parsec_two_dim_block_t * desc = (parsec_two_dim_block_t *)dc;

        va_start(ap, dc);
        i = va_arg(ap, int64_t);
        j = va_arg(ap, int64_t);
        va_end(ap);

        return (uint32_t)(i*desc->nt+j);
    }

    void
    parsec_two_dim_block_key_to_coordinates(parsec_data_collection_t *dc,
                                            parsec_data_key_t key, int64_t *i, int64_t *j)
    {
        parsec_two_dim_block_t * desc = (parsec_two_dim_block_t *)dc;
        *i = key / desc->nt;
        *j = key % desc->nt;

    }

    uint32_t
    parsec_two_dim_block_rank_of_key(parsec_data_collection_t* dc, parsec_data_key_t key)
    {
        int64_t i, j;
        parsec_two_dim_block_key_to_coordinates(dc, key, &i, &j);
        return parsec_two_dim_block_rank_of(dc, i, j);
    }

    int32_t
    parsec_two_dim_block_vpid_of_key(parsec_data_collection_t* dc, parsec_data_key_t key)
    {
        int64_t i, j;
        parsec_two_dim_block_key_to_coordinates(dc, key, &i, &j);
        return parsec_two_dim_block_vpid_of(dc, i, j);
    }

   parsec_data_t *
   parsec_two_dim_block_data_of_key(parsec_data_collection_t* dc, parsec_data_key_t key)
   {
      int64_t i, j;
      parsec_two_dim_block_key_to_coordinates(dc, key, &i, &j);
      return parsec_two_dim_block_data_of(dc, i, j);
   }

   int
   parsec_two_dim_block_key_to_string(
         struct parsec_data_collection_s* desc, parsec_data_key_t datakey,
         char * buffer, uint32_t buffer_size)
   {
      int res;
      (void)desc;

      res = snprintf(buffer, buffer_size, "(%llu)", datakey);
      if (res < 0) {
         printf("error in key_to_string for data collection (%llu) key: %llu\n", desc->dc_id, datakey);
      }
      return res;
   }
   
   
} // End of namespace parsec
