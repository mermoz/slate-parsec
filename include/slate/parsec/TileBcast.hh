#ifndef PARSEC_TILE_BCAST_HH
#define PARSEC_TILE_BCAST_HH

#include <iostream>

#include "parsec.h"

namespace parsec {

template<typename matrix_t, typename scalar_t>
class TileBcast {
public:

   TileBcast() { }
      
   struct Arguments {
      // source tile row index
      int64_t i;
      // source tile column index
      int64_t j;
      // Destination tile range
      std::list<std::array<int64_t, 4>> dst_ranges;
         
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         i(-1), j(-1), dst_ranges({{-1,-1,-1,-1}}), a(nullptr) { }

      Arguments(int64_t in_i, int64_t in_j, std::array<int64_t, 4> in_dst_range, matrix_t *in_a)
         : i(in_i), j(in_j), dst_ranges({in_dst_range}), a(in_a) { }

      Arguments(int64_t in_i, int64_t in_j, std::list<std::array<int64_t, 4>> in_dst_ranges, matrix_t *in_a)
         : i(in_i), j(in_j), dst_ranges(in_dst_ranges), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }

   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto a = this->args_.a;

      auto i = this->args_.i;
      auto j = this->args_.j;

      const int64_t A_mt = a->mt();
      
      // Find the broadcast root rank.
      int root_rank = a->tileRank(i, j);

      // Key associated with (i,j) tile on root node
      int root_key = a->desc->super.data_key(&(a->desc->super), i, j);
      // dtd_tile associated with (i,j) tile on root node
      auto* aij_dtd_tile = a->desc->dtd_tiles[root_key];
      // Arena index for tile (i,j)
      int aij_arena_index = (a->tileRank(i, j) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      // parsec_datacopy associated with (i,j) tile on root node
      parsec_data_copy_t* aij_data_copy = aij_dtd_tile->data_copy;

      // Make sure data_copy exists on root node
      assert(!(a->tileIsLocal(i, j) && (NULL == aij_data_copy)));

#ifdef PARSEC_DTD_DIST_COLLECTIVES

      // Create empty remote_deps
      // parsec_remote_deps_t *deps = nullptr;
      
      // deps = this->create_remote_deps(root_rank, aij_data_copy);

      // Set of destination ranks
      std::set<int> bcast_set;

      // Iterate through the list of ranges
      for (auto dst_range : this->args_.dst_ranges) {
            
         // Destination tile range
         auto i1 = dst_range[0]; 
         auto i2 = dst_range[1]; 
         auto j1 = dst_range[2]; 
         auto j2 = dst_range[3]; 

         // Look if there are tiles in the input range
         if ((i2 < i1) || (j2 < j1))
            return;

         int mpi_rank = a->mpiRank();
         
         auto submat = a->sub(i1, i2, j1, j2);

         // Retrieve rank list
         submat.getRanks(&bcast_set);

         // Add destination rank to braodcast in remote_deps
         // for (auto bcast_rank : bcast_set) {

         //    std::cout << "[TileBcast::submit] my rank = " << a->mpiRank()
         //              << ", i = " << i << ", j = " << j
         //              << ", bcast_rank = " << bcast_rank << std::endl;

         //       // // If data is local to the source tile, nothing to do
         //    // if (bcast_rank == root_rank)
         //    //    continue;

         //    // this->add_remote_dep(bcast_rank, deps); 
         // }
      }

      // Remove root rank from `bcast_set`
      bcast_set.erase(root_rank);
      
      // Convert the set of ranks to a vector.
      std::vector<int> bcast_vec(bcast_set.begin(), bcast_set.end());

      parsec_dtd_broadcast_id(
            j*A_mt+i,
            tp, a->mpiRank(), root_rank,
            aij_dtd_tile, aij_arena_index,
            &bcast_vec[0], bcast_vec.size());
      
      // Now that the remote_deps is created, we can insert the tasks
      // corresponding to the broadcast

      // int dest_rank = root_rank;

      // // Tile is local so it is stored in parsec::LAPACK_TILE format
      // parsec_task_t *bcast_task_root = parsec_dtd_taskpool_create_task(
      //       tp, parsec_cpu_func, priority, "bcast_send_task",
      //       PASSED_BY_REF, aij_dtd_tile, INOUT | parsec::LAPACK_TILE,
      //       sizeof(int), &root_rank, VALUE | AFFINITY,
      //       sizeof(int), &dest_rank, VALUE,
      //       // sizeof(int*), &data_value_out, VALUE,
      //       sizeof(int), &i, VALUE,
      //       sizeof(int), &j, VALUE,
      //       PARSEC_DTD_ARG_END);

      // parsec_dtd_task_t *dtd_bcast_task_root = (parsec_dtd_task_t *)bcast_task_root;
      // std::cout << "[TileBcast::submit] ========== BCAST SEND ========== "
      //           << " rank = " << a->mpiRank()
      //           << ", key = " << dtd_bcast_task_root->ht_item.key << std::endl;
      // dtd_bcast_task_root->deps = deps;
      // // dtd_bcast_task_root->deps = NULL; // Debug
      // // Insert broadcast task on the sending node
      // parsec_insert_dtd_task(bcast_task_root);

      // int index, offset;
      // index = my_rank / (8 * sizeof(uint32_t));
      // offset = my_rank % (8 * sizeof(uint32_t));

      // struct remote_dep_output_param_s* output = &deps->output[0];
      
      // for (auto dest_rank : bcast_set) {

      //    if (dest_rank == root_rank)
      //       continue;

      //    // std::cout << "[TileBcast::submit] my rank = " << a->mpiRank()
      //    //           << ", j = " << j
      //    //           << ", dest_rank = " << dest_rank
      //    //           << std::endl;

      //    // int aij_arena_index = (a->tileRank(k, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      //    // Tile is remote so it is stored in parsec::FULL_TILE format
      //    assert(dest_rank != root_rank);

      //    parsec_task_t *bcast_task_recv = parsec_dtd_taskpool_create_task(
      //          tp, parsec_cpu_func, priority, "bcast_recv_task",
      //          PASSED_BY_REF, aij_dtd_tile, INPUT | parsec::FULL_TILE,
      //          sizeof(int), &root_rank, VALUE,
      //          sizeof(int), &dest_rank, VALUE | AFFINITY,
      //          // sizeof(int*), &data_value_out, VALUE,
      //          sizeof(int), &i, VALUE,
      //          sizeof(int), &j, VALUE,
      //          PARSEC_DTD_ARG_END);

      //    parsec_dtd_task_t *dtd_bcast_task_recv = (parsec_dtd_task_t *)bcast_task_recv;
      //    dtd_bcast_task_recv->super.locals[2].value = dtd_bcast_task_root->ht_item.key; 
      //    std::cout << "[TileBcast::submit] ========== BCAST RECV ========== "
      //              << " rank = " << a->mpiRank()
      //              << ", key = " << dtd_bcast_task_recv->ht_item.key << std::endl;
      //    dtd_bcast_task_recv->deps = deps;
      //    // dtd_bcast_task_root->deps = NULL; // Debug
         
      //    // Insert broadcast task on the sending node
      //    parsec_insert_dtd_task(bcast_task_recv);

      // }

#endif
      
   }

private:

   Arguments args_;

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      // int64_t k;
      // matrix_t *a;
      scalar_t *aij_data;
      int root_rank;
      int dest_rank;
      int i, j;         
      parsec_dtd_unpack_args(
            this_task, &aij_data, &root_rank, &dest_rank,
            &i, &j);

      // std::cout << "[TileBcast::parsec_cpu_func] dest_rank = " << dest_rank << std::endl;
      std::cout << "[TileBcast::parsec_cpu_func] A(" << i << ", " << j <<  ")"
                << ", root_rank = " << root_rank 
                << ", dest_rank = " << dest_rank 
                << std::endl;

      // cpu_func(k, a, akk_data);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }

   // void add_remote_dep(
   //       int dest_rank, parsec_remote_deps_t *deps) {

   //    // Do not add root node to destination node
   //    if (deps->root == dest_rank) return;

   //    struct remote_dep_output_param_s* output = &deps->output[0];
   //    int _array_pos, _array_mask;

   //    // deps->max_priority++;

   //    _array_pos = dest_rank / (8 * sizeof(uint32_t));
   //    _array_mask = 1 << (dest_rank % (8 * sizeof(uint32_t)));

   //    if( !(output->rank_bits[_array_pos] & _array_mask) ) {
   //       output->rank_bits[_array_pos] |= _array_mask;
   //       output->deps_mask |= (1 << 0);
   //       output->count_bits++;
   //    }

   // }
   
   // parsec_remote_deps_t * create_remote_deps(
   //       int root, /* Root node index */
   //       parsec_data_copy_t *data_copy /* data_copy associated with
   //                                        tile to be broadcasted */
   //       ) {

   //    parsec_remote_deps_t *deps = (parsec_remote_deps_t*)remote_deps_allocate(&parsec_remote_dep_context.freelist);
   //    deps->root = root;
   //    deps->outgoing_mask |= (1 << 0);
   //    deps->max_priority  = 0;
      
   //    struct remote_dep_output_param_s* output = &deps->output[0];
   //    output->data.data   = NULL;
   //    output->data.arena  = parsec_dtd_arenas_datatypes[parsec::FULL_TILE].arena;
   //    output->data.layout = parsec_dtd_arenas_datatypes[parsec::FULL_TILE].opaque_dtt;
   //    // output->data.arena  = parsec_dtd_arenas_datatypes[parsec::LAPACK_TILE].arena;
   //    // output->data.layout = parsec_dtd_arenas_datatypes[parsec::LAPACK_TILE].opaque_dtt;
   //    output->data.count  = 1;
   //    output->data.displ  = 0;
   //    output->priority    = 0;

   //    if (this->args_.a->mpiRank() == root) {
   //       output->data.data = data_copy;

   //       // If tile is local, it should be stored in a lapack format
   //       output->data.arena  = parsec_dtd_arenas_datatypes[parsec::LAPACK_TILE].arena;
   //       output->data.layout = parsec_dtd_arenas_datatypes[parsec::LAPACK_TILE].opaque_dtt;
   //    }
   //    /* printf("[create_remote_deps_odd] rank = %d, data_copy = %p\n", rank, data_copy); */

   //    // int _array_pos, _array_mask;
   //    // uint32_t dest_rank;
   //    // for (dest_rank = 0; dest_rank < (uint32_t)world; ++dest_rank) {
   //    //    // Skip if we are root or if the node index is even
   //    //    if((deps->root == dest_rank) || (dest_rank % 2 == 0)) continue;

   //    //    // deps->max_priority++;

   //    //    _array_pos = dest_rank / (8 * sizeof(uint32_t));
   //    //    _array_mask = 1 << (dest_rank % (8 * sizeof(uint32_t)));

   //    //    if( !(output->rank_bits[_array_pos] & _array_mask) ) {
   //    //       output->rank_bits[_array_pos] |= _array_mask;
   //    //       output->deps_mask |= (1 << 0);
   //    //       output->count_bits++;
   //    //    }
   //    // }

   //    return deps;
   // }
   
};

} // End of namespace parsec

#endif /* PARSEC_TILE_BCAST_HH */
